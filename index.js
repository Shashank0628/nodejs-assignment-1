const express = require('express')
const routes = require('./routes/index')
const dotenv = require('dotenv')
const app = express()

dotenv.config()

app.use(routes)

app.listen(process.env.PORT, () => {
    console.log("Server is running")
})